# ZRAM-SYSTEMD is no longer being maintained

- zram-systemd-version: sep 2023

- build-latest: 0.0.1

- Support for the distro: Ubuntu/Debian/Arch/Manjaro

- If you are using a distribution based on Ubuntu, Debian, or Arch, the script will work without any issues, but the distribution must have systemd.

- ZRAM-SYSTEMD is an open-source project, and we are happy to share it with the community. You have complete freedom to do whatever you want with ZRAM-SYSTEMD, in accordance with the terms of the MIT license. You can modify, distribute, use it in your own projects, or even create a fork of ZRAM-SYSTEMD to add additional features.

- zram-systemd version adapted for a distro that uses runit, access this GitLab link: https://gitlab.com/manoel-linux1/zram-runit

## Installation

- To install ZRAM-SYSTEMD, follow the steps below:

# 1. Clone this repository by running the following command

- git clone https://gitlab.com/manoel-linux1/zram-systemd.git

# 2. To install the ZRAM-SYSTEMD script, follow these steps

- chmod a+x `installupdate.sh`

- sudo `./installupdate.sh`

# For check version

sudo `zram-systemd-version` or `zram-systemd-version`

# For change the zram-systemd configuration

sudo `zram-systemd-change`

# For stop the zram-systemd

sudo `zram-systemd-stop`

# For start the zram-systemd

sudo `zram-systemd-start`

# For uninstall

- chmod a+x `uninstall.sh`

- sudo `./uninstall.sh`

# Other Projects

- If you found this project interesting, be sure to check out my other open-source projects on GitLab. I've developed a variety of tools and scripts to enhance the Linux/BSD experience and improve system administration. You can find these projects and more on my GitLab: https://gitlab.com/manoel-linux1

# Project Status

- The ZRAM-SYSTEMD project is currently in development. The latest stable version is 0.0.1. We aim to provide regular updates and add more features in the future.

# License

- ZRAM-SYSTEMD is licensed under the MIT License. See the LICENSE file for more information.

# Acknowledgements

- We would like to thank the open-source community for their support and the libraries used in the development of ZRAM-SYSTEMD.